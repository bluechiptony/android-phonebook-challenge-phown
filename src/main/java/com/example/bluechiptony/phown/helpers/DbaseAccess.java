package com.example.bluechiptony.phown.helpers;

import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.example.bluechiptony.phown.model.Contact;

import java.util.ArrayList;

/**
 * Created by BluechipTony on 07/03/2015. helper class to connect to sqlite database;
 */
public class DbaseAccess extends SQLiteOpenHelper {

    private static final int  database_version = 1;

    private static final String database_name = "PhownbookContacts";

    private static final String contacts_table = "ContactsTable";

    private static final String KEY_ID = "id";

    private static final String KEY_FIRST_NAME = "first_name";
    private static final String KEY_LAST_NAME = "last_name";
    private static final String KEY_PHONE_NUMBER = "phone_number";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_ADDRESS = "address";
    private static final String KEY_PHOTO= "photo";





    public DbaseAccess(Context context){
        super(context, database_name, null, database_version);

    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        String create_table = "CREATE TABLE "+ contacts_table + "(" + KEY_ID +" INTEGER PRIMARY KEY, "
                + KEY_FIRST_NAME +" TEXT,"
                + KEY_LAST_NAME + " TEXT,"
                + KEY_PHONE_NUMBER + " TEXT,"
                + KEY_EMAIL + " TEXT,"
                + KEY_ADDRESS + " TEXT,"
                + KEY_PHOTO + " TEXT)";


        db.execSQL(create_table);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String drop_table = "DROP TABLE IF EXISTS "+database_name;
        try {
            db.execSQL(drop_table);


        }catch (Exception e){

        }

        onCreate(db);

    }



    public void updateContact(Contact contact){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_FIRST_NAME, contact.getFirst_name());
        values.put(KEY_LAST_NAME, contact.getLast_name());
        values.put(KEY_PHONE_NUMBER, contact.getPhone_numbers());
        values.put(KEY_EMAIL, contact.getEmail_address());
        values.put(KEY_ADDRESS, contact.getAddress());

        int result = db.update(contacts_table, values, KEY_ID+" = ?", new String[]{String.valueOf(contact.getDb_id())});
    }
    public void deleteContact(Contact contact){
        SQLiteDatabase db = this.getWritableDatabase();

       // String query = "DELETE FROM "+database_name+"WHERE "+KEY_ID+ " = ?"+Integer.toString(contact.getDb_id());

        db.delete(contacts_table, KEY_ID+"= ?",new String[]{String.valueOf(contact.getDb_id())} );
        db.close();

    }

    public void addContact(Contact contact){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_FIRST_NAME, contact.getFirst_name());
        values.put(KEY_LAST_NAME, contact.getLast_name());
        values.put(KEY_PHONE_NUMBER, contact.getPhone_numbers());
        values.put(KEY_EMAIL, contact.getEmail_address());
        values.put(KEY_ADDRESS, contact.getAddress());

        long result = db.insert(contacts_table, null, values);
        if (result > 0){
            Log.d("dbHelper", "Inserted successfully");
        }else{
            Log.d("dbHelper", "not inserted");
        }
        db.close();

    }


    public Contact[] getAllContacts(){
        ArrayList retuned_contacts = new ArrayList<Contact>();


        String query = "SELECT * FROM "+contacts_table;

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        //loop through rows of result set cursor
        if(cursor.moveToFirst()){
            do {
                Contact current_contact = new Contact();
                current_contact.setDb_id(cursor.getInt(0));
                current_contact.setFirst_name(cursor.getString(1));
                current_contact.setLast_name(cursor.getString(2));
                current_contact.setPhone_numbers(cursor.getString(3));
                current_contact.setEmail_address(cursor.getString(4));
                current_contact.setAddress(cursor.getString(5));

                //current_contact.setPhoto(cursor.getBlob(6));

                retuned_contacts.add(current_contact);
            }while (cursor.moveToNext());
        }

        db.close();
        Contact[] cons = new Contact[retuned_contacts.size()];
        retuned_contacts.toArray(cons);
        return cons;

    }


}
