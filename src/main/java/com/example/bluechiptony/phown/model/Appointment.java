package com.example.bluechiptony.phown.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by BluechipTony on 07/03/2015.
 */
public class Appointment implements Serializable {
    private String app_name;
    private Contact app_contact;
    private Date app_date;
    private int app_prioity;
    private String app_venue;

    public Appointment(String app_name, Contact app_contact, int app_prioity, String app_venue) {
        this.app_name = app_name;
        this.app_contact = app_contact;
        this.app_prioity = app_prioity;
        this.app_venue = app_venue;
    }

    public Appointment(String app_name, Contact app_contact, Date app_date, int app_prioity, String app_venue) {
        this.app_name = app_name;
        this.app_contact = app_contact;
        this.app_date = app_date;
        this.app_prioity = app_prioity;
        this.app_venue = app_venue;
    }

    public String getApp_name() {
        return app_name;
    }

    public void setApp_name(String app_name) {
        this.app_name = app_name;
    }

    public Contact getApp_contact() {
        return app_contact;
    }

    public void setApp_contact(Contact app_contact) {
        this.app_contact = app_contact;
    }

    public Date getApp_date() {
        return app_date;
    }

    public void setApp_date(Date app_date) {
        this.app_date = app_date;
    }

    public int getApp_prioity() {
        return app_prioity;
    }

    public void setApp_prioity(int app_prioity) {
        this.app_prioity = app_prioity;
    }

    public String getApp_venue() {
        return app_venue;
    }

    public void setApp_venue(String app_venue) {
        this.app_venue = app_venue;
    }
}
