package com.example.bluechiptony.phown.views;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bluechiptony.phown.R;

import com.example.bluechiptony.phown.helpers.DbaseAccess;
import com.example.bluechiptony.phown.model.Contact;
import com.snappydb.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URI;


public class AddContact extends ActionBarActivity {

    ImageView img_view;

    TextView first_name, last_name, phone_number, email, address;

    Button add_con;


    //variables for camera and gallery
    private static final int CAMERA_REQUEST = 1888;
    private static int RESULT_LOAD_IMAGE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        img_view = (ImageView)findViewById(R.id.add_con_image);
        registerForContextMenu(img_view);

        prepareView();
        //getActionBar().setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //method created to set necessary resources


    public void prepareView(){
        ImageView img_view = (ImageView)findViewById(R.id.add_con_image);
        TextView txt = (TextView)findViewById(R.id.add_con_text);

        first_name = (TextView)findViewById(R.id.firstNameText);
        last_name = (TextView)findViewById(R.id.lastNameText);
        phone_number = (TextView)findViewById(R.id.phoneNumberText);
        email = (TextView)findViewById(R.id.email_addy);
        address = (TextView)findViewById(R.id.addressText);

        add_con = (Button)findViewById(R.id.add_con_but);

        add_con.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addContactSql();

                finish();
            }
        });

        img_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplication(), "Load Image Action", Toast.LENGTH_LONG).show();

                /*

                */
            }
        });

        txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplication(), "Load Image Action", Toast.LENGTH_LONG).show();
            }
        });
    }


    //overidden methods for image gallery or camera context menu

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);


        menu.setHeaderTitle("Choose Image Source");
        menu.add(0, v.getId(), 0, "Camera");
        menu.add(0, v.getId(), 0, "Gallery");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if(item.getTitle() == "Camera"){
            LoadCamera();
        }else if(item.getTitle() == "Gallery"){
            LoadGallery();
        }
        return super.onContextItemSelected(item);


    }


    public void LoadCamera(){
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    public void LoadGallery(){
        Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, RESULT_LOAD_IMAGE);
    }


    //onactivity result



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == CAMERA_REQUEST){

            Bitmap thumbnail = (Bitmap)data.getExtras().get("data");

            img_view.setImageBitmap(thumbnail);


        }else if(requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data){
            Uri selected_image = data.getData();

            String [] file_path_column = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selected_image,
                    file_path_column, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(file_path_column[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            img_view.setImageBitmap(BitmapFactory.decodeFile(picturePath));


        }
    }


    public void addContactSql(){
        Contact temp_contact = new Contact();

        temp_contact.setFirst_name(first_name.getText().toString());
        temp_contact.setLast_name(last_name.getText().toString());
        temp_contact.setPhone_numbers(phone_number.getText().toString());
        temp_contact.setEmail_address(email.getText().toString());
        temp_contact.setAddress(address.getText().toString());

        DbaseAccess dba = new DbaseAccess(this);

        dba.addContact(temp_contact);


    }


    //snap db to add contact
    public void addContact(Contact contact){

        try{

            DB snappyDB = DBFactory.open(getBaseContext(), "contacts");
            snappyDB.put("cons", contact);
            snappyDB.close();

        }catch (SnappydbException sne){

        }
    }




    public void saveBitmap(Bitmap image){


        File file = new File(Environment.getExternalStorageDirectory()+File.separator+"image.jpg");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        try{
            file.createNewFile();
            FileOutputStream fo = new FileOutputStream(file);
            fo.write(bytes.toByteArray());
            fo.close();
        }catch (Exception e){

        }
    }


}
