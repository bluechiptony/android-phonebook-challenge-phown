package com.example.bluechiptony.phown.views;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.view.MenuInflater;
import android.widget.Toast;

import com.example.bluechiptony.phown.R;
import com.example.bluechiptony.phown.helpers.ContactListAdapter;
import com.example.bluechiptony.phown.helpers.DbaseAccess;
import com.example.bluechiptony.phown.model.Contact;

import java.util.ArrayList;
import java.util.Arrays;


/*
    Action bar activity class to display all contacts- First screen on Application load

 */

public class ContactList extends ActionBarActivity {



    EditText search_text_box;
    //Contact [] contaks = get_staticContatct();

    ListView contlist;
    Contact contaks[];



    public Contact[] get_staticContatct(){

        Contact[] contaks = new Contact[10];

        contaks[0] = new Contact("James", "Dean", "09876778654", "jadear@hoopla.com", "44 holloway drive, ellington");
        contaks[1] = new Contact("Rico", "Suave", "078456772634", "jadear@hoopla.com", "44 holloway drive, ellington");
        contaks[2] = new Contact("Jack", "Alexis", "09848903909", "jadear@hoopla.com", "44 holloway drive, ellington");
        contaks[3] = new Contact("Juan", "Marquez", "09873398654", "jadear@hoopla.com", "44 holloway drive, ellington");
        contaks[4] = new Contact("Mellisa", "Milano", "08834678652", "jadear@hoopla.com", "44 holloway drive, ellington");
        contaks[5] = new Contact("James", "Mollan", "098767328897",  "jadear@hoopla.com", "44 holloway drive, ellington");
        contaks[6] = new Contact("Alexander", "Estebaros", "0387637655",  "jadear@hoopla.com", "44 holloway drive, ellington");
        contaks[7] = new Contact("Rachael", "Malone", "09876778654", "jadear@hoopla.com", "44 holloway drive, ellington");
        contaks[8] = new Contact("Cameron", "Dean", "09876778654",  "jadear@hoopla.com", "44 holloway drive, ellington");
        contaks[9] = new Contact("Jamal", "Henderson", "09876778654", "jadear@hoopla.com", "44 holloway drive, ellington");

        return contaks;

    }

    public void loadAllContacts(){

        contaks = new DbaseAccess(this).getAllContacts();
        if(contaks == null){
            contaks = new Contact[2];
            contaks[0] = new Contact("James", "Dean", "09876778654", "jadear@hoopla.com", "44 holloway drive, ellington");
            contaks[1] = new Contact("Rico", "Suave", "078456772634", "jadear@hoopla.com", "44 holloway drive, ellington");
        }else{

        }
    }


    public void prepView(){
        loadAllContacts();

        search_text_box = (EditText)findViewById(R.id.search_text_box);
        //configure list
        ListAdapter ladt = new ContactListAdapter(this, contaks);
        contlist = (ListView) findViewById(R.id.ConList);
        contlist.setAdapter(ladt);



        registerForContextMenu(contlist);





        //configure on item click for list view
        contlist.setOnItemClickListener(
                new AdapterView.OnItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String clname = String.valueOf(parent.getItemAtPosition(position));

                        Contact pCon = (Contact)parent.getItemAtPosition(position);
                        //Toast.makeText(MainScreen.this, "Wahgwan "+clname, Toast.LENGTH_LONG).show();

                        Intent intent = new Intent(ContactList.this, ViewContactActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("GName", clname);
                        intent.putExtra("Conta", (java.io.Serializable) pCon);
                        startActivity(intent);
                    }
                }
        );


        search_text_box.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                String datext = search_text_box.getText().toString();
                if(datext == null){

                }else if(datext  ==""){

                }else if(datext.length() >0 ){
                    //Toast.makeText(getApplicationContext(), "You typed "+datext, Toast.LENGTH_LONG).show();
                    reloadArray(datext);
                }


            }
        });
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);


        prepView();




    }


    @Override
    protected void onResume() {
        super.onResume();
        prepView();
    }

    public void reloadArray(String search){

        Log.d("Loster", search);
        ArrayList<Contact> templist = new ArrayList<Contact>();

        for(int i = 0; i < contaks.length; i++){
            if(contaks[i].getFirst_name().toLowerCase().startsWith(search.toLowerCase())){
                templist.add(contaks[i]);
            }
        }

        Contact[] cons = new Contact[templist.size()];
        templist.toArray(cons);

        for (int i = 0; i < cons.length; i++){
            System.out.println("Name con: "+cons[i].getFirst_name());
        }


        //contlist = (ListView)findViewById(R.id.)

        /*

        ListAdapter ladt = new ContactListAdapter(this, contaks);

        contlist = (ListView) findViewById(R.id.ConList);
        contlist.setAdapter(ladt);

*/
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_contact_list, menu);
        return true;
    }


    //added to create menu for long press

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        /*
        menu.setHeaderTitle("Select Your Action");
        menu.add(0, v.getId(), 0, "Call");
        menu.add(0, v.getId(), 0, "Make Appointment");
        menu.add(0, v.getId(), 0, "Edit");
        menu.add(0, v.getId(), 0, "Delete");

        */
        //display context menu for long press click

        //init string array for context menu items
        String[] menuitems = {"Call", "Make Appointment", "Edit", "Delete"};

        //check if longpress is from contact list
        if (v.getId() == R.id.ConList) {

            ListView lv = (ListView)v;
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;


            Contact con = (Contact)lv.getItemAtPosition(info.position);
            //add menuitems to context men
            menu.setHeaderTitle("Choose Action for "+con.getFirst_name()+" "+con.getLast_name());
            menu.add(0, v.getId(), 0, "Call");
            menu.add(0, v.getId(), 0, "Send Email");
            menu.add(0, v.getId(), 0, "Make Appointment");

            menu.add(0, v.getId(), 0, "Edit");
            menu.add(0, v.getId(), 0, "Delete");


        }


    }




    //overidden method to handle context menu selected

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();

        String list_id = Long.toString(menuInfo.id);


        Contact press_contact = contaks[Integer.parseInt(list_id)];

        if(item.getTitle()=="Call"){

            String iid =  Integer.toString(item.getItemId());
           // Toast.makeText(getApplicationContext(), item.getTitle()+" Position: "+list_id+", "+press_contact.getFirst_name(), Toast.LENGTH_SHORT).show();

            //start call activity via new intent
            Intent intent = new Intent(Intent.ACTION_CALL);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("tel:" + press_contact.getPhone_numbers()));
            getApplicationContext().startActivity(intent);

        }else if(item.getTitle() =="Edit"){
            //Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();
            openEditActivity(press_contact);
        }else if(item.getTitle()=="Send Email"){
            openSendEmailActivity(press_contact);
        }else if(item.getTitle()=="Delete"){
              //Toast.makeText(getApplicationContext(), item.getTitle(), Toast.LENGTH_SHORT).show();


            deleteContact(press_contact);


        }


        return super.onContextItemSelected(item);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        String toastMsg;

        //actionbar item selected handler

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if (id == R.id.add_contact_button){
            openAddActivity();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onOptionsMenuClosed(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_contact_list, menu);

        super.onOptionsMenuClosed(menu);
    }


    //method created to open new AddContact class via intent

    public void openAddActivity(){
        Intent addIntent = new Intent(ContactList.this, AddContact.class);

        addIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(addIntent);
    }


    //open new edit activity
    public void openEditActivity(Contact con){
        Intent intent = new Intent(ContactList.this, ViewContactActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("GName", con.getFirst_name()+ " "+ con.getLast_name());
        intent.putExtra("Conta", (java.io.Serializable) con);
        startActivity(intent);
    }


    //method to send email to contact via new activity
    public void openSendEmailActivity(Contact con){

        Intent intent = new Intent(ContactList.this, SendEmailActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("GName", con.getFirst_name()+ " "+ con.getLast_name());
        intent.putExtra("Conta", (java.io.Serializable) con);
        startActivity(intent);
    }


    public void deleteContact(Contact con){
        android.app.FragmentManager fragman = getFragmentManager();

        DeleteDialog dd = new DeleteDialog();
        Bundle args = new Bundle();
        args.putSerializable("passed_con", con);
        dd.setArguments(args);
        dd.show(fragman, "Del dia");
    }
}
