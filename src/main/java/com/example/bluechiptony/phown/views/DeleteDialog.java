package com.example.bluechiptony.phown.views;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.Toast;

import com.example.bluechiptony.phown.R;
import com.example.bluechiptony.phown.helpers.DbaseAccess;
import com.example.bluechiptony.phown.model.Contact;

/**
 * Created by BluechipTony on 10/03/2015.
 */
public class DeleteDialog extends android.app.DialogFragment{

    Contact req_contact;


    public Dialog onCreateDialog(Bundle savedInstanceState){

        //req_contact = (Contact)savedInstanceState.getSerializable("passed_con");

       // Bundle rq = getArguments().getBundle();
        req_contact = (Contact)getArguments().getSerializable("passed_con");

        //Contact pass_con = savedInstanceState.

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Delete "+req_contact.getFirst_name()+ " ?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Toast.makeText(getActivity(), "Deleted", Toast.LENGTH_LONG).show();
                new DbaseAccess(getActivity()).deleteContact(req_contact);
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //Toast.makeText(getActivity(), "NO Delete", Toast.LENGTH_LONG).show();
            }
        });
        return builder.create();
    }

}
