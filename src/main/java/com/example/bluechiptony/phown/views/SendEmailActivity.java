package com.example.bluechiptony.phown.views;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.example.bluechiptony.phown.R;
import com.example.bluechiptony.phown.model.Contact;

public class SendEmailActivity extends ActionBarActivity {

    EditText email_tbox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_email);
        getpassedContactAndLoad();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_send_email, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void getpassedContactAndLoad(){
        Intent intent = getIntent();
        String theName = intent.getStringExtra("GName");
        final Contact gCon = (Contact)intent.getSerializableExtra("Conta");

        setTitle(gCon.getFirst_name()+ " "+ gCon.getLast_name());

        email_tbox = (EditText)findViewById(R.id.emaill_address_to);
        email_tbox.setText(gCon.getEmail_address().toString());
    }
}
