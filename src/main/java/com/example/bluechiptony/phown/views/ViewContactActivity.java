package com.example.bluechiptony.phown.views;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.bluechiptony.phown.R;
import com.example.bluechiptony.phown.model.Contact;

public class ViewContactActivity extends ActionBarActivity {

    public Contact reCon;


    EditText fnam, lname, phone1, phone2, email, address, pcode;

    Button saveButton, deleteButton;
    ImageButton make_call, new_email, new_app;
    public ViewContactActivity(Contact reCon) {
        this.reCon = reCon;
    }

    public ViewContactActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_contact);
        loadContactDetails();
    }



    public void loadContactDetails(){

        Intent intent = getIntent();
        String theName = intent.getStringExtra("GName");
        final Contact gCon = (Contact)intent.getSerializableExtra("Conta");

        fnam = (EditText)findViewById(R.id.firstNameText);


        lname = (EditText)findViewById(R.id.lastNameText);
        phone1 = (EditText)findViewById(R.id.phoneNumberText);
        email = (EditText)findViewById(R.id.email_addy);
        //email = (EditText)findViewById(R.id.em);
        address = (EditText)findViewById(R.id.addressText);



        //set onclick listener for make call
        make_call = (ImageButton)findViewById(R.id.make_call_button);

        make_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setData(Uri.parse("tel:" + gCon.getPhone_numbers()));
                getApplicationContext().startActivity(intent);
            }
        });


        //set onclick listener for ne email
        new_email = (ImageButton)findViewById(R.id.new_email);
        new_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewContactActivity.this, SendEmailActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("GName", gCon.getFirst_name()+ " "+ gCon.getLast_name());
                intent.putExtra("Conta", (java.io.Serializable) gCon);
                startActivity(intent);
            }
        });

        deleteButton = (Button)findViewById(R.id.deleteButton);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteContact(gCon);
                //finish();
            }
        });

        fnam.setText(gCon.getFirst_name());
        lname.setText(gCon.getLast_name());
        phone1.setText(gCon.getPhone_numbers());
        email.setText(gCon.getEmail_address());
        address.setText(gCon.getAddress());

        disableTextBoxes();

        ImageView conImage = (ImageView) findViewById(R.id.imageView);


        conImage.setImageResource(R.drawable.funksmile);

        setTitle(gCon.getFirst_name()+ " "+ gCon.getLast_name());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_view_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if(id == R.id.add_contact_button){
            openAddActivity();

            return true;
        }else if(id == R.id.edit_contact_button){
            enableTextBoxes();
            Toast.makeText(getApplicationContext(), "Edit function", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void disableTextBoxes(){
        fnam = (EditText)findViewById(R.id.firstNameText);
        lname = (EditText)findViewById(R.id.lastNameText);
        phone1 = (EditText)findViewById(R.id.phoneNumberText);
        email = (EditText)findViewById(R.id.email_addy);
        //email = (EditText)findViewById(R.id.em);
        address = (EditText)findViewById(R.id.addressText);
        saveButton = (Button)findViewById(R.id.saveButton);
        deleteButton = (Button)findViewById(R.id.deleteButton);

        fnam.setEnabled(false);
        lname.setEnabled(false);
        phone1.setEnabled(false);
        email.setEnabled(false);
        address.setEnabled(false);
        saveButton.setEnabled(false);

        saveButton.setVisibility(View.INVISIBLE);





    }

    public void enableTextBoxes(){
        fnam = (EditText)findViewById(R.id.firstNameText);
        lname = (EditText)findViewById(R.id.lastNameText);
        phone1 = (EditText)findViewById(R.id.phoneNumberText);
        email = (EditText)findViewById(R.id.email_addy);
        //email = (EditText)findViewById(R.id.em);
        address = (EditText)findViewById(R.id.addressText);
        saveButton = (Button)findViewById(R.id.saveButton);


        saveButton.setVisibility(View.VISIBLE);
        fnam.setEnabled(true);
        lname.setEnabled(true);
        phone1.setEnabled(true);
        email.setEnabled(true);
        address.setEnabled(true);
        saveButton.setEnabled(true);


    }

    public void openAddActivity(){
        Intent addIntent = new Intent(ViewContactActivity.this, AddContact.class);

        addIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(addIntent);
    }

    public void deleteContact(Contact con){
        android.app.FragmentManager fragman = getFragmentManager();

        DeleteDialog dd = new DeleteDialog();
        Bundle args = new Bundle();
        args.putSerializable("passed_con", con);
        dd.setArguments(args);
        dd.show(fragman, "Del dia");
    }
}
